import time

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as clr


"""
import math
"""

#Pour afficher l'image.
def f_affiche(image):
    plt.imshow(image)
    plt.axis('off')
    plt.show()


#On introduit les fonctions : la fonction, sa derivee première pour Newton,
#et seconde pour Householder, qui nous demande h(x)

if bool(input("La fonction est un polynôme (ne rien écrire si non) : ")) :
    degré = int(input("degré du polynôme : "))
    Coeff = [float(input("Coefficient pour X puissance 0 : "))]
    polynome = str(Coeff[0])
    for i in range(1,degré + 1) :
        Coeff.append(float(input("Coefficient pour la "+str(i)+"-ème puissance : ")))
        polynome = (str(Coeff[i])+" * X^"+str(i)+" + "+polynome)
    print(polynome)
    def f(x) :
        r = 0
        for i in range(degré+1) :
            r = r + Coeff[i]*(x**i)
        return r
    
    def fp(x) :
        r = 0
        for i in range(1,degré+1) :
            r = r + i*Coeff[i]*(x**(i-1))
        return r

    def fs(x) :
        r = 0
        for i in range(2,degré+1) :
            r = r + i*(i-1)*Coeff[i]*(x**(i-2))
        return r
else :
    def f(x):
        return x**10-1

    def fp(x):
        return 10*x**9

    def fs(x):
        return 90*x**8

'''
def f(x):
    return x**3-1

def fp(x):
    return 3*x**2

def fs(x):
    return 6*x
'''
def h(x):
    return (f(x)*fs(x))/(2*fp(x)**2)

#On introduit epsilon, le nombre d'iteration maximum, et le critere pour savoir
#si l'on considère qu'on explose ou non.

epsilon=float(input("Précision : "))
n_max =int(input("Itérations max : "))
explosion =int(input("Valeur d'explosion minimale : "))

#On introduit l'ensemble des solutions pour pouvoir associer chaque couleur à une unique solution

sol=[]

couleurs = np.zeros((10,n_max,3))

def init_couleur():
    moitie = int(n_max/2)
    for i in range(10):
        couleurs[i,moitie] = np.array(clr.to_rgb(list(clr.TABLEAU_COLORS.values())[i]))
        for k in range(n_max-1):
            temp = clr.rgb_to_hsv(couleurs[i,moitie])
            if k >= moitie:
                temp[2] = temp[2] * (k+1)/n_max
                k += 1
            else:
                temp[1] = temp[1] * (1 - (k+1)/n_max)
            couleurs[i,k] = np.array(clr.hsv_to_rgb(temp))*255
        couleurs[i,moitie] *= 255


#On introduit le cadre d'etudes. l'intervalle pour Im(z) et Re(z).
#Puis le nombre de cases dans notre array pour avoir un degre de precision.

P_reel_min= float(input("Partie réelle minimum : "))
P_reel_max = float(input("Partie réelle maximum : "))
P_im_min = float(input("Partie imaginaire minimum : "))
P_im_max = float(input("Partie imaginaire maximum : "))
nombre_colonne = int(input("Nombre de points en abscisse : "))
nombre_ligne = int((P_im_max-P_im_min)/(P_reel_max - P_reel_min) * nombre_colonne)

#On introduit notre fameux array. J'ai du mettre le 3 et le dtype=np.uint8
#parce que sinon, ça marche pas

image = np.zeros((nombre_ligne,nombre_colonne,3),dtype=np.uint8)

#On definit à chaque pixel, les coordonnées d'un point de R2
#Attention: les lignes correspondent à y et les colonnes à x

def pixel2coord(x_pix,y_pix):
    x=(P_reel_max-P_im_min)*x_pix/nombre_colonne + P_reel_min
    y=(P_im_max-P_im_min)*y_pix/nombre_ligne + P_im_min
    return (x,y)

#On fait l'opération inverse

def coord2pixel(x,y):
    x_pix = int(nombre_colonne*(x-P_reel_min)/(P_reel_max-P_reel_min))
    y_pix = int(nombre_ligne*(y-P_im_min)/(P_im_max-P_im_min))
    return x_pix,y_pix

#On n'a pas besoin de mettre en argument les fonctions. Elles sont des
#globales, tout comme epsilon. On rajoute un critère nombre d'iteration :
#il va nous servir pour le degrade, et la notion de non convergence
#(boucle infinie, explosion vers l'infini)


#Newton
def conv_newton(x0):
    if abs(x0)>=explosion:
        return "Argument invalide. Veuillez changer l'intervalle d'etude"
    u=x0
    k=0
    if fp(u)==0:
        return u,n_max
    else:
        v=u-f(u)/fp(u)
        while abs(v-u)>epsilon and k<n_max and abs(v)<explosion:
            if fp(v)==0:
                return v,n_max
            else:
                u,v=v,v-f(v)/fp(v)
            k+=1
    return v,k


#Householder
def convergence_householder(x0):
    if abs(x0)>=explosion:
        return "Argument invalide. Veuillez changer l'intervalle d'etude"
    u=x0
    k=0
    if fp(u)==0:
        return u,n_max
    else:
        v=u-(f(u)*(1+h(u)))/fp(u)
        while abs(v-u)>epsilon and k<n_max and abs(v)<explosion:
            if fp(v)==0:
                return v,n_max
            else:
                u,v=v,v-(f(v)*(1+h(v)))/fp(v)
            k+=1
    return v,k


#On cree notre fonction qui a chaque coordonnee d'un array, on la remplie de 3 ints pour le codage RVB. Si la boucle a trop tourne pour la
#fonction Newton, on renvoie blanc pour indiquer la non-convergence. Sinon, on compare la valeur de convergence avec toutes les solutions
#pour lui attribuer une couleur

def couleur_pixel(colonne_pixel,ligne_pixel):
    x,y = pixel2coord(colonne_pixel,ligne_pixel)
    approximation,nombre_iteration = conv_newton(complex(x,y))

    if nombre_iteration>=n_max or abs(approximation)>=explosion:
        image[ligne_pixel,colonne_pixel] = [255,255,255] #Ne converge pas
    else:
        i=0
        while i < len(sol) and abs(approximation - sol[i]) >= 2*epsilon:
            i+=1
        if i >= len(sol):
            sol.append(approximation)
        image[ligne_pixel,colonne_pixel] = couleurs[i,nombre_iteration]


def fractale_newton():
    ligne,colonne = 0,0
    for colonne in range(nombre_colonne):
        for ligne in range(nombre_ligne):
            couleur_pixel(colonne,ligne)


#On exécute et on affiche.

init_couleur()

fractale_newton()
f_affiche(image)

